#!/usr/bin/env python

import os
import zipfile

with zipfile.ZipFile("successor-tabs.xpi", "w") as z:
    for dirname, subdirs, files in os.walk("src"):
        subdirs.sort()
        files.sort()
        for filename in files:
            if not filename.startswith("."):
                path = os.path.join(dirname, filename)
                zi = zipfile.ZipInfo.from_file(path)
                zi.date_time = (1980, 1, 1, 0, 0, 0)
                zi.filename = os.path.relpath(path, "src")
                with open(path, "rb") as f:
                    z.writestr(zi, f.read(), zipfile.ZIP_DEFLATED, 9)
