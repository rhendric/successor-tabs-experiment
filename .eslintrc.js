"use strict";

module.exports = {
  plugins: [
    "mozilla",
  ],
  extends: [
    "plugin:mozilla/recommended",
  ],
  rules: {
    "array-bracket-spacing": "error",
    "comma-dangle": ["error", "always-multiline"],
    "indent": ["error", 2, {
      CallExpression: {
        arguments: "first",
      },
    }],
    "no-unused-vars": [
      "error",
      {
        args: "after-used",
        vars: "all",
      },
    ],
    "object-curly-spacing": "error",
    "strict": ["error", "global"],
    "mozilla/mark-exported-symbols-as-used": "error",
  },
  overrides: [
    {
      files: ["**/.eslintrc.js", "src/api/SuccessorTabsModel.js"],
      globals: {
        module: true,
      },
    },
    {
      files: ["test/**"],
      parserOptions: {
        sourceType: "module",
      },
    },
  ],
};
