"use strict";

/* global ExtensionUtils, ExtensionAPI */
/* import-globals-from ./SuccessorTabsModel.js */

const {ExtensionError} = ExtensionUtils;
const {Management: {global: {tabTracker, windowTracker}}} =
  ChromeUtils.import("resource://gre/modules/Extension.jsm", {});

ChromeUtils.import("resource://gre/modules/Services.jsm");
ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");

XPCOMUtils.defineLazyServiceGetter(this, "resProto",
                                   "@mozilla.org/network/protocol;1?name=resource",
                                   "nsISubstitutingProtocolHandler");

this.successorTabs = class extends ExtensionAPI {
  constructor(extension) {
    super(extension);

    // Silly shenanigans to import ./SuccessorTabsModel.js, which is only its
    // own file so that it can be unit tested. If there's a better way to do
    // this, great.
    const apiURI = Services.io.newURI("api/", null, extension.rootURI);
    resProto.setSubstitutionWithFlags("successortabs", apiURI, resProto.ALLOW_CONTENT_ACCESS);
    Cu.unload("resource://successortabs/SuccessorTabsModel.js");
    /* eslint-disable-next-line no-unused-vars */
    ChromeUtils.import("resource://successortabs/SuccessorTabsModel.js");
    resProto.setSubstitution("successortabs", null);

    /**
     * This is a handler for remove and detach events which ensures that when a
     * tab is taken from a window by either of those methods, any predecessors
     * of the departing tab adopt the departing tab's successor as their own.
     * This prevents holes in the successor graph and contains connected
     * subgraphs to a single window.
     *
     * Unlike most of the code in this file, this code is intended to be kept
     * in some form if this API is uplifted.
     */
    this.makePredecessorsAdoptSuccessor = (eventName, {nativeTab, tabId, isWindowClosing}) => {
      // If the window is closing, successors won't matter.
      if (isWindowClosing) {
        return;
      }

      this.successorTabsModel.makePredecessorsOfNativeTabAdopt(nativeTab, this.successorTabsModel.getSuccessorIdOfNativeTab(nativeTab));

      // In case this is a detach, explicitly clear this tab's successor.
      this.successorTabsModel.updateSuccessorMaps(tabId, nativeTab, TAB_ID_NONE, null);
    };

    const xapi = this;

    /**
     * This patches the tabbrowser code responsible for selecting the
     * next tab to activate after an active tab is closed.
     */
    const tabbrowserPatches = this.tabbrowserPatches = new Patcher({
      _findTabToBlurTo(aTab) {
        if (!aTab.selected) {
          return null;
        }

        const successorId = xapi.successorTabsModel.getSuccessorIdOfNativeTab(aTab);
        if (successorId !== TAB_ID_NONE) {
          console.log(`Resolving tab ${successorId}`);
          const tab = tabTracker.getTab(successorId, null);
          if (tab !== null) {
            console.log(`Found tab ${tab.label}`);
            return tab;
          }
          console.warn(`Tab ${successorId} is not valid; falling back`);
        }

        return this[tabbrowserPatches.original._findTabToBlurTo](aTab);
      },
    });

    this.patchFindTabToBlurTo = (window) => tabbrowserPatches.patch(window.gBrowser);

    /**
     * This patches the TabTracker code that emits the tab-activated
     * event (which becomes the onActivated API event) to include the
     * previously active tab.
     */
    const tabTrackerPatches = this.tabTrackerPatches = new Patcher({
      emitActivated(nativeTab, previousTab) {
        this.emit("tab-activated", {
          tabId: this.getId(nativeTab),
          previousTabId: previousTab.closing ? null : this.getId(previousTab, null),
          windowId: windowTracker.getId(nativeTab.ownerGlobal)});
      },

      handleEvent(event) {
        if (event.type === "TabSelect") {
          const nativeTab = event.target;
          Promise.resolve().then(() => {
            this.emitActivated(nativeTab, event.detail.previousTab);
          });
        } else {
          this[tabTrackerPatches.original.handleEvent](event);
        }
      },
    });
  }

  maintainLineOfSuccession() {
    tabTracker.on("tab-removed", this.makePredecessorsAdoptSuccessor);
    tabTracker.on("tab-detached", this.makePredecessorsAdoptSuccessor);
  }

  abandonLineOfSuccession() {
    tabTracker.off("tab-removed", this.makePredecessorsAdoptSuccessor);
    tabTracker.off("tab-detached", this.makePredecessorsAdoptSuccessor);
  }

  patchAllWindows() {
    windowTracker.addOpenListener(this.patchFindTabToBlurTo);
    for (const window of windowTracker.browserWindows()) {
      this.tabbrowserPatches.patch(window.gBrowser);
    }
  }

  unpatchAllWindows() {
    windowTracker.removeOpenListener(this.patchFindTabToBlurTo);
    for (const window of windowTracker.browserWindows()) {
      this.tabbrowserPatches.unpatch(window.gBrowser);
    }
  }

  onStartup() {
    this.successorTabsModel = makeSuccessorTabsModel({tabTracker, ExtensionError});
    this.maintainLineOfSuccession();
    this.patchAllWindows();
    this.tabTrackerPatches.patch(tabTracker);
  }

  getAPI() {
    // Please consider these functions for inclusion into the `tabs`
    // namespace; `successorTabs` is used here because I couldn't figure out
    // how to extend an existing API namespace without replacing it entirely.
    return {successorTabs: this.successorTabsModel.api};
  }

  onShutdown() {
    this.successorTabsModel = null;
    this.abandonLineOfSuccession();
    this.unpatchAllWindows();
    this.tabTrackerPatches.unpatch(tabTracker);
  }
};


/**
 * This is a small monkeypatching utility that would not need to be present if
 * this API is uplifted into Firefox.
 */
class Patcher {
  constructor(replacements) {
    this.replacements = replacements;
    const original = this.original = {};
    for (const key in replacements) {
      original[key] = Symbol(`original ${key}`);
    }
  }

  patch(object) {
    let warned = false;
    for (const key in this.original) {
      const original = this.original[key];
      if (!object[original]) {
        object[original] = object[key];
        object[key] = this.replacements[key];
      } else if (!warned) {
        console.warn("Attempted to patch this object a second time", object);
        warned = true;
      }
    }
  }

  unpatch(object) {
    let warned = false;
    for (const key in this.original) {
      const original = this.original[key];
      if (object[original]) {
        delete object[key];
        if (!object[key]) {
          object[key] = object[original];
        }
        delete object[original];
      } else if (!warned) {
        console.warn("Attempted to unpatch this object a second time", object);
        warned = true;
      }
    }
  }
}
