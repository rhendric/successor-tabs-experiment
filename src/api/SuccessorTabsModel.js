"use strict";

const TAB_ID_NONE = -1;

function makeSuccessorTabsModel({tabTracker, ExtensionError}) {
  /**
   * This is a map from native tab instances to tab IDs. The successor of a
   * tab T is the tab that should be activated when T is closed while active.
   * Tabs that don't have a defined successor will be absent from this map;
   * for such tabs, the default Firefox behavior will determine the next tab
   * to activate.
   *
   * (This and the following are WeakMaps keyed on native tab instances for
   * this experiment, but if this is uplifted into Firefox, it might be better
   * to replace them with fields on tabs instead.)
   */
  const successorMap = new WeakMap();

  /**
   * This is a map from native tab instances to Sets of tab IDs. The
   * predecessors of a tab T are the tabs that have T as their successor.
   *
   * updateSuccessorMaps maintains the invariants that
   *
   *   predecessorMap.get(tabTracker.getTab(successorMap.get(t))).has(getId(t))
   *
   * for all tab instances t where successorMap.has(t), and similarly
   *
   *   successorMap.get(tabTracker.getTab(p)) === getId(t)
   *
   * for all tab instances t where p is in predecessorMap.get(t).
   */
  const predecessorMap = new WeakMap();

  return {
    // These functions are exposed through the API...
    api: {
      getSuccessor, setSuccessor, moveInSuccession, moveInSuccessionAfter,
    },

    // ... whereas these are internal to the patched parent process
    // code...
    getSuccessorIdOfNativeTab,
    makePredecessorsOfNativeTabAdopt,
    updateSuccessorMaps,

    // ... and these values are exposed only for testing.
    successorMap,
    predecessorMap,
  };

  function getSuccessorIdOfNativeTab(tab) {
    return successorMap.has(tab) ? successorMap.get(tab) : TAB_ID_NONE;
  }

  function updateSuccessorMaps(tabId, tab, successorTabId, successorTab) {
    // Both tabId and tab are required here. If a tab is closed, tabTracker
    // will have already forgotten the association between tab and ID by the
    // time this function needs to be used to update our maps.

    /* istanbul ignore if */
    if (tabId === successorTabId) {
      // This should be guarded against elsewhere in the API; seeing this
      // message probably indicates a bug.
      console.warn("Tab can't succeed itself; clearing successor instead");
      successorTabId = TAB_ID_NONE;
    }

    // Remove this tab from the predecessor set of its current successor, if any.
    if (successorMap.has(tab)) {
      const currentSuccessor = tabTracker.getTab(successorMap.get(tab), null);
      if (currentSuccessor) {
        predecessorMap.get(currentSuccessor).delete(tabId);
      }
    }

    if (successorTabId === TAB_ID_NONE) {
      successorMap.delete(tab);
    } else {
      successorMap.set(tab, successorTabId);

      // Add this tab to the predecessor set of its new successor.
      let predecessors = predecessorMap.get(successorTab);
      if (!predecessors) {
        predecessorMap.set(successorTab, predecessors = new Set());
      }
      predecessors.add(tabId);
    }
  }

  function makePredecessorsOfNativeTabAdopt(nativeTab, adopteeTabId) {
    const predecessorTabIds = predecessorMap.get(nativeTab);
    if (predecessorTabIds) {
      for (const predecessorTabId of predecessorTabIds) {
        const predecessor = tabTracker.getTab(predecessorTabId, null);
        if (predecessor !== null) {
          // Cycles are allowed in the successor graph, but no tab can be its
          // own successor. Therefore we have to allow here for the possibility
          // that the adoptee tab may also be a predecessor of nativeTab, and
          // unset that tab's successor instead of setting it to itself.
          let successorTabId = predecessorTabId === adopteeTabId ? TAB_ID_NONE : adopteeTabId;
          const successorTab = successorTabId === TAB_ID_NONE ? null : tabTracker.getTab(successorTabId, null);
          if (successorTab === null) {
            successorTabId = TAB_ID_NONE;
          }
          updateSuccessorMaps(predecessorTabId, predecessor, successorTabId, successorTab);
        }
      }
    }
  }

  function getSuccessor(tabId) {
    return getSuccessorIdOfNativeTab(tabTracker.getTab(tabId));
  }

  function setSuccessor(tabId, successorTabId) {
    const tab = tabTracker.getTab(tabId); // Throws if tab ID is invalid

    let successor;
    if (successorTabId !== TAB_ID_NONE) {
      successor = tabTracker.getTab(successorTabId, null);
      if (successor === null) {
        throw new ExtensionError(`Invalid successor tab ID: ${successorTabId}`);
      }
      if (tab.ownerGlobal !== successor.ownerGlobal) {
        throw new ExtensionError("Successor tab is not in the same window");
      }
      if (tabId === successorTabId) {
        throw new ExtensionError("A tab can't be its own successor");
      }
    }

    updateSuccessorMaps(tabId, tab, successorTabId, successor);
  }

  function moveInSuccession(tabIds, beforeTabId, insert) {
    const tabIdSet = new Set(tabIds);
    if (tabIdSet.size !== tabIds.length) {
      throw new ExtensionError("IDs must not occur more than once in tabIds");
    }
    if (insert && tabIdSet.has(beforeTabId)) {
      throw new ExtensionError("Value of beforeTabId must not occur in tabIds if insert is true");
    }

    let beforeTab = null;
    if (beforeTabId !== TAB_ID_NONE) {
      beforeTab = tabTracker.getTab(beforeTabId, null);
      if (beforeTab === null) {
        beforeTabId = TAB_ID_NONE;
      }
    }
    let referenceWindow = beforeTab && beforeTab.ownerGlobal;

    let previousTabId, previousTab, firstTabId;
    for (const tabId of tabIds) {
      const tab = tabTracker.getTab(tabId, null);
      if (tab === null) {
        continue;
      }
      if (referenceWindow === null) {
        referenceWindow = tab.ownerGlobal;
      } else if (tab.ownerGlobal !== referenceWindow) {
        continue;
      }
      makePredecessorsOfNativeTabAdopt(tab, getSuccessorIdOfNativeTab(tab));
      if (previousTab) {
        updateSuccessorMaps(previousTabId, previousTab, tabId, tab);
      } else {
        firstTabId = tabId;
      }
      previousTabId = tabId;
      previousTab = tab;
    }

    if (previousTab) {
      if (beforeTab !== null && insert) {
        makePredecessorsOfNativeTabAdopt(beforeTab, firstTabId);
      }
      if (previousTabId !== beforeTabId) {
        updateSuccessorMaps(previousTabId, previousTab, beforeTabId, beforeTab);
      }
    }
  }

  function moveInSuccessionAfter(afterTabId, tabIds, insert) {
    const tabIdSet = new Set(tabIds);
    if (tabIdSet.size !== tabIds.length) {
      throw new ExtensionError("IDs must not occur more than once in tabIds");
    }
    if (tabIdSet.has(afterTabId)) {
      throw new ExtensionError("Value of afterTabId must not occur in tabIds");
    }

    let previousTabId = afterTabId, previousTab = tabTracker.getTab(afterTabId, null);
    let lastSuccessorId = TAB_ID_NONE, lastSuccessor = null;
    if (insert && previousTab !== null) {
      lastSuccessorId = getSuccessorIdOfNativeTab(previousTab);
      lastSuccessor = tabTracker.getTab(lastSuccessorId, null);
      if (lastSuccessor === null) {
        lastSuccessorId = TAB_ID_NONE;
      }
    }
    let referenceWindow = previousTab && previousTab.ownerGlobal;
    for (const tabId of tabIds) {
      const tab = tabTracker.getTab(tabId, null);
      if (tab === null) {
        continue;
      }
      if (referenceWindow === null) {
        referenceWindow = tab.ownerGlobal;
      } else if (tab.ownerGlobal !== referenceWindow) {
        continue;
      }
      makePredecessorsOfNativeTabAdopt(tab, getSuccessorIdOfNativeTab(tab));
      if (previousTab) {
        updateSuccessorMaps(previousTabId, previousTab, tabId, tab);
      }
      previousTabId = tabId;
      previousTab = tab;
    }
    if (previousTab) {
      if (lastSuccessorId === previousTabId) {
        lastSuccessorId = TAB_ID_NONE;
        lastSuccessor = null;
      }
      updateSuccessorMaps(previousTabId, previousTab, lastSuccessorId, lastSuccessor);
    }
  }
}

const EXPORTED_SYMBOLS = ["TAB_ID_NONE", "makeSuccessorTabsModel"];

/* istanbul ignore else */
if (typeof module !== "undefined") {
  module.exports = {TAB_ID_NONE, makeSuccessorTabsModel};
}
