"use strict";

Object.assign(browser.tabs, browser.successorTabs);

// Force the API to register
browser.tabs.getSuccessor(0).catch(() => {});


browser.tabs.onActivated.addListener(({tabId, previousTabId}) => {
  {
    const readingList = readingListsByMember.get(tabId);
    if (readingList) {
      removeFromReadingList(readingList, tabId);
      if (previousTabId != null) {
        const chain = [tabId, ...readingList];
        browser.tabs.moveInSuccession(chain, previousTabId);
      }
      return;
    }
  }

  if (previousTabId != null) {
    const readingList = readingListsByOpener.get(tabId);
    if (readingList) {
      browser.tabs.moveInSuccession([tabId, ...readingList], previousTabId);
      return;
    }

    browser.tabs.moveInSuccession([tabId], previousTabId);
  }
});


browser.tabs.onCreated.addListener(({id: tabId, openerTabId}) => {
  if (tabId === browser.tabs.TAB_ID_NONE || openerTabId == null) {
    return;
  }
  let readingList = readingListsByOpener.get(openerTabId);
  let targetTabId;
  if (readingList) {
    targetTabId = readingList[readingList.length - 1];
    readingList.push(tabId);
  } else {
    targetTabId = openerTabId;
    readingListsByOpener.set(openerTabId, readingList = [tabId]);
    readingList.opener = openerTabId;
  }
  browser.tabs.moveInSuccessionAfter(targetTabId, [tabId], true);
  readingListsByMember.set(tabId, readingList);
});


function onDeparted(tabId) {
  if (tabId === browser.tabs.TAB_ID_NONE) {
    return;
  }

  let readingList;
  if ((readingList = readingListsByMember.get(tabId))) {
    removeFromReadingList(readingList, tabId);
  } else if ((readingList = readingListsByOpener.get(tabId))) {
    delete readingList.opener;
    readingListsByOpener.delete(tabId);
  }
}
browser.tabs.onRemoved.addListener(onDeparted);
browser.tabs.onDetached.addListener(onDeparted);

function removeFromReadingList(readingList, tabId) {
  const index = readingList.indexOf(tabId);
  readingList.splice(index, 1);
  readingListsByMember.delete(tabId);
  if (readingList.length === 0 && readingList.opener != null) {
    readingListsByOpener.delete(readingList.opener);
  }
}


const readingListsByOpener = new Map();
const readingListsByMember = new Map();
