import test from "ava";

import {TAB_ID_NONE, makeSuccessorTabsModel} from "../src/api/SuccessorTabsModel";

const tabsById = new Map();
let nextId = 0;

const tabTracker = {
  getTab(id, notFound) {
    if (tabsById.has(id)) {
      return tabsById.get(id);
    }
    if (notFound === void 0) {
      throw new Error(`Invalid tab ID: ${id}`);
    } else {
      return notFound;
    }
  },
};

test.beforeEach(() => {
  tabsById.clear();
  nextId = 0;
});

test("set and get", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  checkInvariants(t, model);
  makeWindowWithTabs(4);

  t.is(model.api.getSuccessor(0), TAB_ID_NONE);

  model.api.setSuccessor(1, 2);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(1), 2);

  model.api.setSuccessor(2, 3);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(2), 3);

  model.api.setSuccessor(2, TAB_ID_NONE);
  t.is(model.api.getSuccessor(2), TAB_ID_NONE);
});

test("setSuccessor validation", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(2);

  t.throws(() => model.api.setSuccessor(2, 0), "Invalid tab ID: 2");
  t.throws(() => model.api.setSuccessor(0, 2), "Invalid successor tab ID: 2");
  t.throws(() => model.api.setSuccessor(0, 0), "A tab can't be its own successor");

  tabTracker.getTab(1).ownerGlobal = {};

  t.throws(() => model.api.setSuccessor(0, 1), "Successor tab is not in the same window");
});

test("getSuccessor validation", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});

  t.throws(() => model.api.getSuccessor(0), "Invalid tab ID: 0");
});

test("makePredecessorsOfNativeTabAdopt", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(4);

  model.api.setSuccessor(0, 2);
  model.api.setSuccessor(1, 2);
  model.api.setSuccessor(2, 3);
  model.api.setSuccessor(3, 1);

  model.makePredecessorsOfNativeTabAdopt(tabTracker.getTab(2), 3);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 3);
  t.is(model.api.getSuccessor(1), 3);

  model.makePredecessorsOfNativeTabAdopt(tabTracker.getTab(1), 3);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 3);
  t.is(model.api.getSuccessor(3), TAB_ID_NONE);
});

test("makePredecessorsOfNativeTabAdopt is resilient to missing tabs", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(3);

  model.api.setSuccessor(0, 1);
  model.api.setSuccessor(1, 2);

  tabsById.delete(0);

  model.makePredecessorsOfNativeTabAdopt(tabTracker.getTab(1), 2);
  checkInvariants(t, model);
});

test("moveInSuccession", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(8);

  model.api.moveInSuccession([1, 0], 0);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), TAB_ID_NONE);
  t.is(model.api.getSuccessor(1), 0);

  model.api.moveInSuccession([0, 1, 2, 3], 0);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 3);
  t.is(model.api.getSuccessor(3), 0);

  model.api.setSuccessor(7, 0);
  model.api.moveInSuccession([4, 5, 6, 7], TAB_ID_NONE);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(4), 5);
  t.is(model.api.getSuccessor(5), 6);
  t.is(model.api.getSuccessor(6), 7);
  t.is(model.api.getSuccessor(7), TAB_ID_NONE);

  model.api.setSuccessor(0, 7);
  model.api.setSuccessor(1, 2);
  model.api.setSuccessor(2, 3);
  model.api.setSuccessor(3, 4);
  model.api.setSuccessor(4, 3);
  model.api.setSuccessor(5, 6);
  model.api.setSuccessor(6, 7);
  model.api.setSuccessor(7, 5);

  model.api.moveInSuccession([4, 6, 3, 2], 7);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 7);
  t.is(model.api.getSuccessor(1), TAB_ID_NONE);
  t.is(model.api.getSuccessor(2), 7);
  t.is(model.api.getSuccessor(3), 2);
  t.is(model.api.getSuccessor(4), 6);
  t.is(model.api.getSuccessor(5), 7);
  t.is(model.api.getSuccessor(6), 3);
  t.is(model.api.getSuccessor(7), 5);
});

test("moveInSuccession, insert=true", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(8);

  model.api.setSuccessor(0, 7);
  model.api.setSuccessor(1, 2);
  model.api.setSuccessor(2, 3);
  model.api.setSuccessor(3, 4);
  model.api.setSuccessor(4, 3);
  model.api.setSuccessor(5, 6);
  model.api.setSuccessor(6, 7);
  model.api.setSuccessor(7, 5);

  model.api.moveInSuccession([4, 6, 3, 2], 7, true);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 4);
  t.is(model.api.getSuccessor(1), TAB_ID_NONE);
  t.is(model.api.getSuccessor(2), 7);
  t.is(model.api.getSuccessor(3), 2);
  t.is(model.api.getSuccessor(4), 6);
  t.is(model.api.getSuccessor(5), 4);
  t.is(model.api.getSuccessor(6), 3);
  t.is(model.api.getSuccessor(7), 5);
});

test("moveInSuccession validation", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(2);
  t.throws(() => model.api.moveInSuccession([1, 1], 0), "IDs must not occur more than once in tabIds");
  t.throws(() => model.api.moveInSuccession([0, 1], 0, true), "Value of beforeTabId must not occur in tabIds if insert is true");
});

test("moveInSuccession is resilient to missing tabs", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(4);

  model.api.moveInSuccession([0, 3, 1, 2], TAB_ID_NONE);
  tabsById.delete(1);

  model.api.moveInSuccession([0, 1, 2, 3], TAB_ID_NONE);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 2);
  t.is(model.api.getSuccessor(2), 3);
  t.is(model.api.getSuccessor(3), TAB_ID_NONE);

  model.api.moveInSuccession([0, 3, 2], 1);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 3);
  t.is(model.api.getSuccessor(2), TAB_ID_NONE);
  t.is(model.api.getSuccessor(3), 2);

  // All tabs missing is a special case.
  tabsById.delete(0);
  model.api.moveInSuccession([0], 1);
  checkInvariants(t, model);
});

test("moveInSuccession is resilient to different windows", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(3);
  makeWindowWithTabs(3);

  model.api.moveInSuccession([0, 1, 2], 0);
  model.api.moveInSuccession([3, 4, 5], 3);

  model.api.moveInSuccession([1, 4, 3], 5);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 0);
  t.is(model.api.getSuccessor(3), 5);
  t.is(model.api.getSuccessor(4), 3);
  t.is(model.api.getSuccessor(5), TAB_ID_NONE);

  model.api.moveInSuccession([0, 1, 2], 0);
  model.api.moveInSuccession([3, 4, 5], 3);

  model.api.moveInSuccession([4, 1, 3], 5);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 0);
  t.is(model.api.getSuccessor(3), 5);
  t.is(model.api.getSuccessor(4), 3);
  t.is(model.api.getSuccessor(5), TAB_ID_NONE);

  model.api.moveInSuccession([0, 1, 2], 0);
  model.api.moveInSuccession([3, 4, 5], 3);

  model.api.moveInSuccession([4, 1, 3], TAB_ID_NONE);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 0);
  t.is(model.api.getSuccessor(3), TAB_ID_NONE);
  t.is(model.api.getSuccessor(4), 3);
  t.is(model.api.getSuccessor(5), TAB_ID_NONE);
});

test("moveInSuccessionAfter", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(8);

  model.api.moveInSuccessionAfter(0, [1]);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), TAB_ID_NONE);

  model.api.setSuccessor(3, 4);
  model.api.moveInSuccessionAfter(0, [1, 2, 3]);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 3);
  t.is(model.api.getSuccessor(3), TAB_ID_NONE);

  model.api.setSuccessor(0, 7);
  model.api.setSuccessor(1, 2);
  model.api.setSuccessor(2, 3);
  model.api.setSuccessor(3, 4);
  model.api.setSuccessor(4, 3);
  model.api.setSuccessor(5, 6);
  model.api.setSuccessor(6, 7);
  model.api.setSuccessor(7, 5);

  model.api.moveInSuccessionAfter(7, [4, 6, 3, 2]);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 7);
  t.is(model.api.getSuccessor(1), TAB_ID_NONE);
  t.is(model.api.getSuccessor(2), TAB_ID_NONE);
  t.is(model.api.getSuccessor(3), 2);
  t.is(model.api.getSuccessor(4), 6);
  t.is(model.api.getSuccessor(5), 7);
  t.is(model.api.getSuccessor(6), 3);
  t.is(model.api.getSuccessor(7), 4);
});

test("moveInSuccessionAfter, insert=true", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(8);

  model.api.setSuccessor(0, 7);
  model.api.setSuccessor(1, 2);
  model.api.setSuccessor(2, 3);
  model.api.setSuccessor(3, 4);
  model.api.setSuccessor(4, 3);
  model.api.setSuccessor(5, 6);
  model.api.setSuccessor(6, 7);
  model.api.setSuccessor(7, 5);

  model.api.moveInSuccessionAfter(7, [4, 6, 3, 2], true);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 7);
  t.is(model.api.getSuccessor(1), TAB_ID_NONE);
  t.is(model.api.getSuccessor(2), 5);
  t.is(model.api.getSuccessor(3), 2);
  t.is(model.api.getSuccessor(4), 6);
  t.is(model.api.getSuccessor(5), 7);
  t.is(model.api.getSuccessor(6), 3);
  t.is(model.api.getSuccessor(7), 4);

  model.api.moveInSuccessionAfter(7, [0, 4], true);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 4);
  t.is(model.api.getSuccessor(4), TAB_ID_NONE);
  t.is(model.api.getSuccessor(7), 0);
});

test("moveInSuccessionAfter validation", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(3);
  t.throws(() => model.api.moveInSuccessionAfter(0, [1, 1]), "IDs must not occur more than once in tabIds");
  t.throws(() => model.api.moveInSuccessionAfter(0, [0, 1]), "Value of afterTabId must not occur in tabIds");
});

test("moveInSuccessionAfter is resilient to missing tabs", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(4);

  model.api.moveInSuccessionAfter(0, [3, 1, 2]);
  tabsById.delete(1);

  model.api.moveInSuccessionAfter(0, [1, 2, 3]);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 2);
  t.is(model.api.getSuccessor(2), 3);
  t.is(model.api.getSuccessor(3), TAB_ID_NONE);

  // All tabs missing is a special case.
  tabsById.delete(0);
  model.api.moveInSuccessionAfter(0, [1]);
  checkInvariants(t, model);
});

test("moveInSuccessionAfter, insert=true is resilient to missing tabs", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(5);

  model.api.moveInSuccessionAfter(0, [3, 1, 4, 2]);
  tabsById.delete(3);

  model.api.moveInSuccessionAfter(0, [1, 2, 4], true);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 4);
  t.is(model.api.getSuccessor(4), TAB_ID_NONE);

  tabsById.delete(0);
  model.api.moveInSuccessionAfter(0, [4, 1], true);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(1), TAB_ID_NONE);
  t.is(model.api.getSuccessor(2), TAB_ID_NONE);
  t.is(model.api.getSuccessor(4), 1);
});

test("moveInSuccessionAfter is resilient to different windows", t => {
  const model = makeSuccessorTabsModel({tabTracker, ExtensionError: Error});
  makeWindowWithTabs(3);
  makeWindowWithTabs(3);

  model.api.moveInSuccession([0, 1, 2], 0);
  model.api.moveInSuccession([3, 4, 5], 3);

  model.api.moveInSuccessionAfter(5, [1, 4, 3]);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 0);
  t.is(model.api.getSuccessor(3), TAB_ID_NONE);
  t.is(model.api.getSuccessor(4), 3);
  t.is(model.api.getSuccessor(5), 4);

  model.api.moveInSuccession([0, 1, 2], 0);
  model.api.moveInSuccession([3, 4, 5], 3);

  model.api.moveInSuccessionAfter(5, [4, 1, 3]);
  checkInvariants(t, model);
  t.is(model.api.getSuccessor(0), 1);
  t.is(model.api.getSuccessor(1), 2);
  t.is(model.api.getSuccessor(2), 0);
  t.is(model.api.getSuccessor(3), TAB_ID_NONE);
  t.is(model.api.getSuccessor(4), 3);
  t.is(model.api.getSuccessor(5), 4);
});



function makeWindowWithTabs(count) {
  const ownerGlobal = {};
  for (let i = 0; i < count; i++) {
    const id = nextId++;
    tabsById.set(id, {id, ownerGlobal});
  }
}

function checkInvariants(t, model) {
  for (const tab of tabsById.values()) {
    if (model.successorMap.has(tab)) {
      const stabId = model.successorMap.get(tab);
      const stab = tabTracker.getTab(stabId, null);
      if (stab !== null) {
        t.true(model.predecessorMap.get(stab).has(tab.id));
      }
    }
    if (model.predecessorMap.has(tab)) {
      const ptabIds = model.predecessorMap.get(tab);
      for (const ptabId of ptabIds) {
        const ptab = tabTracker.getTab(ptabId, null);
        if (ptab !== null) {
          t.is(model.successorMap.get(ptab), tab.id);
        }
      }
    }
  }
}
